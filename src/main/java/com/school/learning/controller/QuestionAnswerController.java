package com.school.learning.controller;

import com.school.learning.model.request.QuestionAnswerRequest;
import com.school.learning.model.response.QuestionAnswerResponse;
import com.school.learning.service.questionanswer.QuestionAnswerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/question-answer")
public class QuestionAnswerController {
    @Autowired
    private QuestionAnswerService questionAnswerService;

    @PostMapping
    public ResponseEntity<String> createQuestionAnswer(@RequestBody QuestionAnswerRequest request) {
        questionAnswerService.insertQuestionAnswer(request);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("/{questionId}/student/{studentId}")
    public ResponseEntity<QuestionAnswerResponse> getQuestionAndAnswerByStudent(
            @PathVariable Long questionId,
            @PathVariable Long studentId
    ) {
        QuestionAnswerResponse questionAnswerDTO = questionAnswerService.getQuestionAndAnswerByStudent(questionId, studentId);
        return ResponseEntity.ok(questionAnswerDTO);
    }
}
