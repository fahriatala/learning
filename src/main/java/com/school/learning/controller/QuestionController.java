package com.school.learning.controller;

import com.school.learning.model.request.QuestionAnswerRequest;
import com.school.learning.model.request.QuestionRequest;
import com.school.learning.model.response.QuestionResponse;
import com.school.learning.service.question.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/question")
public class QuestionController {
    @Autowired
    private QuestionService questionService;

    @PostMapping
    @Transactional
    public ResponseEntity<Void> insertTeacher(@RequestBody QuestionRequest request){
        questionService.insertQuestion(request);
        return ResponseEntity.status(201).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<QuestionResponse> getQuestionById(@PathVariable Long id) {
        QuestionResponse response = questionService.getQuestionById(id);
        return ResponseEntity.ok(response);
    }
}
