package com.school.learning.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.school.learning.model.data.StudentData;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class QuestionAnswerResponse {
    private Long questionId;
    private String questionTitle;
    private String questionDescription;
    private String questionText;
    private String className;

    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "Asia/Jakarta")
    private LocalDateTime endTime;

    private String studentAnswer;
    private StudentData studentData;
    private Double score;
}
