package com.school.learning.model.data;

import lombok.Data;

@Data
public class StudentData {
    private Long id;
    private String fullName;
}
