package com.school.learning.model.data;

import lombok.Data;

@Data
public class TeacherData {
    private Long id;
    private String fullName;
}
