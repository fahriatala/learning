package com.school.learning.model.request;
import lombok.Data;

@Data
public class QuestionAnswerRequest {
    private String questionAnswers;
    private Long questionId;
    private Long studentId;
}
