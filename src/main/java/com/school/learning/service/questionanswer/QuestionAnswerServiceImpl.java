package com.school.learning.service.questionanswer;

import com.school.learning.feign.StudentFeignClient;
import com.school.learning.model.data.StudentData;
import com.school.learning.model.entity.Question;
import com.school.learning.model.entity.QuestionAnswer;
import com.school.learning.model.request.QuestionAnswerRequest;
import com.school.learning.model.response.QuestionAnswerResponse;
import com.school.learning.repository.QuestionAnswerRepository;
import com.school.learning.repository.QuestionRepository;
import com.shared.library.AppException;
import com.shared.library.DataNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class QuestionAnswerServiceImpl implements QuestionAnswerService {
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private StudentFeignClient studentFeignClient;
    @Autowired
    private QuestionAnswerRepository questionAnswerRepository;

    @Override
    @Transactional
    public void insertQuestionAnswer(QuestionAnswerRequest request) {
        Optional<Question> optionalQuestion = questionRepository.findById(request.getQuestionId());
        if (!optionalQuestion.isPresent()) {
            throw new DataNotFoundException("Question not found");
        }

        boolean isValidStudent = studentFeignClient.validateStudent(request.getStudentId());
        if (!isValidStudent) {
            throw new AppException("Invalid student");
        }

        QuestionAnswer questionAnswer = new QuestionAnswer();
        questionAnswer.setQuestionId(request.getQuestionId());
        questionAnswer.setStudentId(request.getStudentId());
        questionAnswer.setQuestionAnswers(request.getQuestionAnswers());

        questionAnswerRepository.save(questionAnswer);
    }

    @Override
    public QuestionAnswerResponse getQuestionAndAnswerByStudent(Long questionId, Long studentId) {
        Optional<Question> optionalQuestion = questionRepository.findById(questionId);
        if (!optionalQuestion.isPresent()) {
            throw new DataNotFoundException("Question not found");
        }
        Question question = optionalQuestion.get();

        Optional<QuestionAnswer> optionalAnswer = questionAnswerRepository.findByQuestionIdAndStudentId(questionId, studentId);
        if (!optionalAnswer.isPresent()) {
            throw new AppException("Question answer not found for the student");
        }

        QuestionAnswer questionAnswer = optionalAnswer.get();
        StudentData student = studentFeignClient.getStudentById(studentId);

        QuestionAnswerResponse response = new QuestionAnswerResponse();
        response.setQuestionId(questionId);
        response.setQuestionDescription(question.getDescription());
        response.setQuestionTitle(question.getTitle());
        response.setQuestionText(question.getQuestions());
        response.setClassName(question.getClassName());
        response.setEndTime(question.getEndTime());
        response.setStudentData(student);
        response.setScore(questionAnswer.getScore());

        return response;
    }
}
