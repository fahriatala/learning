package com.school.learning.service.questionanswer;

import com.school.learning.model.request.QuestionAnswerRequest;
import com.school.learning.model.response.QuestionAnswerResponse;

public interface QuestionAnswerService {
    void insertQuestionAnswer(QuestionAnswerRequest request);
    QuestionAnswerResponse getQuestionAndAnswerByStudent(Long questionId, Long studentId);
}
