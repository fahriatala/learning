package com.school.learning.service.question;

import com.school.learning.feign.TeacherFeignClient;
import com.school.learning.model.data.TeacherData;
import com.school.learning.model.entity.Question;
import com.school.learning.model.request.QuestionRequest;
import com.school.learning.model.response.QuestionResponse;
import com.school.learning.repository.QuestionRepository;
import com.shared.library.AppException;
import com.shared.library.DataNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class QuestionServiceImpl implements QuestionService  {
    @Autowired
    private QuestionRepository questionRepository;

    @Autowired
    private TeacherFeignClient teacherFeignClient;
    @Override
    public void insertQuestion(QuestionRequest request) {
        boolean isValidTeacher = teacherFeignClient.validateTeacher(request.getTeacherId());

        if (!isValidTeacher) {
            throw new AppException("Invalid teacher");
        }

        Question question = new Question();
        question.setTitle(request.getTitle());
        question.setDescription(request.getDescription());
        question.setTeacherId(request.getTeacherId());
        question.setClassName(request.getClassName());
        question.setEndTime(request.getEndTime());
        question.setQuestions(request.getQuestions());

        // Save the question entity
        questionRepository.save(question);
    }

    @Override
    public QuestionResponse getQuestionById(Long questionId) {
        Optional<Question> optionalQuestion = questionRepository.findById(questionId);
        if (optionalQuestion.isPresent()) {
            Question question = optionalQuestion.get();
            TeacherData teacher = teacherFeignClient.getTeacherById(question.getTeacherId());

            QuestionResponse response = new QuestionResponse();
            response.setId(question.getId());
            response.setTitle(question.getTitle());
            response.setDescription(question.getDescription());
            response.setQuestion(question.getQuestions());
            response.setClassName(question.getClassName());
            response.setEndTime(question.getEndTime());
            response.setTeacher(teacher);
            return response;
        } else {
            throw new DataNotFoundException("Question not found");
        }
    }
}
