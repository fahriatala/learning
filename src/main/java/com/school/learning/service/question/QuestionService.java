package com.school.learning.service.question;

import com.school.learning.model.request.QuestionRequest;
import com.school.learning.model.response.QuestionResponse;

public interface QuestionService {
    void insertQuestion(QuestionRequest request);

    QuestionResponse getQuestionById(Long questionId);
}
