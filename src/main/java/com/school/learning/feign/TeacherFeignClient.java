package com.school.learning.feign;

import com.school.learning.model.data.TeacherData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "TEACHER")
public interface TeacherFeignClient {
    @GetMapping("/api/teacher/exists/{teacherId}")
    boolean validateTeacher(@PathVariable("teacherId") long teacherId);

    @GetMapping("/api/teacher/{teacherId}")
    TeacherData getTeacherById(@PathVariable("teacherId") Long teacherId);
}
