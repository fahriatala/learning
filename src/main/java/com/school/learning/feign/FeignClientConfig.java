package com.school.learning.feign;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackages = "com.school.learning.feign")
public class FeignClientConfig {
    // Feign client configuration
}

