package com.school.learning.feign;

import com.school.learning.model.data.StudentData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "STUDENT")
public interface StudentFeignClient {
    @GetMapping("/api/student/exists/{studentId}")
    boolean validateStudent(@PathVariable("studentId") long studentId);

    @GetMapping("/api/student/{studentId}")
    StudentData getStudentById(@PathVariable("studentId") Long studentId);
}
