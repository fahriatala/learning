## Requirements

This version of application requires the following dependencies

| Name          | Details                  |
|---------------|--------------------------|
| IDE           | Intellij IDE             |
| Java Version  | Java 8                   |
| Framework     | Spring Boot              |
| Database      | MySQL                    |

## Postman Documentation

https://documenter.getpostman.com/view/1388924/2sA3XWeKBF#intro

## How to Run

Run the teacher & student app first. Then while using Intellij IDE, simply just click Run Button.